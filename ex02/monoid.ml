module type MONOID =
	sig 
		type element 
		val zero1 : element
		val zero2 : element
		val mul : element -> element -> element
		val add : element -> element -> element
		val div : element -> element -> element
		val sub : element -> element -> element
	end

module Calc = 
	functor (M:MONOID) -> struct
	let add x y = M.add x y
	let sub x y = M.sub x y
	let mul x y = M.mul x y
	let div x y = M.div x y
	let rec power n p = if p > 0 then  M.mul n (power n (p - 1)) else M.zero2
	let rec fact x = if x = M.zero2 then M.zero2 else M.mul x (fact ( M.sub x M.zero2))
end

module INT =
	struct
		type element = int
		let zero1 = 0
		let zero2 = 1 
		let mul = ( * )
		let add = ( + )
		let div x y = if y = 0 then (x / zero2) else (x / y)
		let sub = ( - )

	end

module FLOAT =
struct
	type element = float
	let zero1 = 0.0
	let zero2 = 1.0 
	let mul = ( *. )
	let add = ( +. )
	let div x y = if y = 0.0 then (x /. zero2) else (x /. y)
	let sub = ( -. )

end

module Calc_int = Calc(INT)
module Calc_float = Calc(FLOAT)

let () =
	print_endline (string_of_int (Calc_int.add 3 3));
	print_endline (string_of_int (Calc_int.add 3 0));
	print_endline (string_of_int (Calc_int.sub 3 3));
	print_endline (string_of_int (Calc_int.sub 3 0));
	print_endline (string_of_int (Calc_int.mul 3 3));
	print_endline (string_of_int (Calc_int.mul 3 1));
	print_endline (string_of_int (Calc_int.div 3 3));
	print_endline (string_of_int (Calc_int.div 3 1));
	print_endline (string_of_int (Calc_int.div 3 0));
	print_endline (string_of_int (Calc_int.power 3 3));
	print_endline (string_of_int (Calc_int.power 3 1));
	print_endline (string_of_int (Calc_int.power 3 0));
	print_endline (string_of_int (Calc_int.fact 3));
	print_endline (string_of_int (Calc_int.fact 5));
	print_endline (string_of_float (Calc_float.add 3.123 3.6));
	print_endline (string_of_float (Calc_float.add 3.123 0.6));
	print_endline (string_of_float (Calc_float.sub 3.123 3.6));
	print_endline (string_of_float (Calc_float.sub 3.123 0.6));
	print_endline (string_of_float (Calc_float.mul 3.123 3.6));
	print_endline (string_of_float (Calc_float.mul 3.123 1.6));
	print_endline (string_of_float (Calc_float.div 3.123 3.6));
	print_endline (string_of_float (Calc_float.div 3.123 1.6));
	print_endline (string_of_float (Calc_float.div 3.123 0.6));
	print_endline (string_of_float (Calc_float.power 3.123 3));
	print_endline (string_of_float (Calc_float.power 3.123 1));
	print_endline (string_of_float (Calc_float.power 3.123 0));
	print_endline (string_of_float (Calc_float.fact 3.));
	print_endline (string_of_float (Calc_float.fact 25.));
	print_endline (string_of_float (Calc_float.power 3.0 3));
	print_endline (string_of_int (Calc_int.mul (Calc_int.add 20 1) 2));
	print_endline (string_of_float (Calc_float.mul (Calc_float.add 20.0 1.0) 2.0))