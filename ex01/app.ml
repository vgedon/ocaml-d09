
module App = 
struct
	type project = string * string * int
	let zero :project = ("", "", 0)
	let combine ((lp, _, lg):project) ((rp, _, rg):project) = ((lp ^ rp, begin if ((lg + rg) /2 ) > 80 then "succeed" else "failed" end, (lg + rg)/2):project)
	let fail ((p,_,_):project) = ((p, "failed", 0):project)
	let success ((p,_,_):project) = ((p, "succeed", 80):project)
end


let () = 
	let print_proj (((p, s, g)):App.project) = print_endline ("Project: " ^ p ^ " => " ^ s ^ " with " ^ (string_of_int g)) in
	let ocaml:App.project = ("OCaml", "succeed", 100) in
	let cpp:App.project = ("C++", "failed", 0) in
	let z = App.zero in
	print_proj ocaml;
	print_proj cpp;
	print_proj z;
	print_proj (App.combine ocaml cpp);
	print_proj (App.fail ocaml);
	print_proj (App.success cpp);